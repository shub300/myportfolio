$(document).ready(function () {
  // typing animation
  (function ($) {
    $.fn.writeText = function (content) {
      var contentArray = content.split(''),
        current = 0,
        elem = this;
      setInterval(function () {
        if (current < contentArray.length) {
          elem.text(elem.text() + contentArray[current++]);
        }
      }, 80);
    };
  })(jQuery);

  // input text for typing animation
  $('#holder').writeText('FULL STACK ENGINEER | B.E (CSE)');

  // initialize wow.js
  new WOW().init();

  // Push the body and the nav over by 285px over
  var main = function () {
    window.dispatchEvent(new Event('resize')); // Trigger resize event onload for setting view automatically

    $('.fa-bars').click(function () {
      $('.nav-screen').animate(
        {
          right: '0px',
        },
        200
      );

      $('body').animate(
        {
          right: '285px',
        },
        200
      );
    });

    // Then push them back */
    $('.fa-times').click(function () {
      $('.nav-screen').animate(
        {
          right: '-285px',
        },
        200
      );

      $('body').animate(
        {
          right: '0px',
        },
        200
      );
    });

    $('.nav-links a').click(function () {
      $('.nav-screen').animate(
        {
          right: '-285px',
        },
        500
      );

      $('body').animate(
        {
          right: '0px',
        },
        500
      );
    });
  };

  $(document).ready(main);

  // initiate full page scroll

  $('#fullpage').fullpage({
    scrollBar: true,
    responsiveWidth: 400,
    navigation: true,
    navigationTooltips: ['HOME', 'ABOUT', 'PROJECTS', 'CONTACT', 'CONNECT'],
    anchors: ['home', 'about', 'projects', 'contact', 'connect'],
    menu: '#myMenu',
    fitToSection: false,

    afterLoad: function (anchorLink, index) {
      var loadedSection = $(this);

      $('.contact-link,.project-link,.about-link,.home-link').removeClass(
        'primary-font'
      );
      $('#fp-nav ul li a span')
        .removeClass('background-black')
        .removeClass('background-white');
      //using index
      if (index == 1) {
        $('.fp-tooltip').addClass('font-white').removeClass('font-black');
        $('#fp-nav ul li a span')
          .removeClass('background-black')
          .addClass('background-white');
        $('#fp-nav ul li a.active span')
          .addClass('background-white')
          .removeClass('background-black');
        /* add opacity to arrow */
        $('.fa-chevron-down').each(function () {
          $(this).css('opacity', '1');
        });
        $('.header-links a').each(function () {
          $(this).css('color', 'white');
        });
      } else if (index != 1) {
        $('.fp-tooltip').addClass('font-black').removeClass('font-white');
        $('#fp-nav ul li a span')
          .addClass('background-black')
          .removeClass('background-white');
        $('#fp-nav ul li a.active span')
          .addClass('background-black')
          .removeClass('background-white');
        $('.header-links a').each(function () {
          $(this).css('color', 'black');
        });
      }

      //using index
      if (index == 2) {
        $('.about-link').addClass('primary-font');
        /* animate skill bars */
        $('.skillbar').each(function () {
          $(this)
            .find('.skillbar-bar')
            .animate(
              {
                width: jQuery(this).attr('data-percent'),
              },
              2500
            );
        });
      } else if (index == 3) {
        $('.project-link').addClass('primary-font');
      } else if (index == 4) {
        $('.contact-link').addClass('primary-font');
      } else if (index == 1) {
        $('.home-link').addClass('primary-font');
      }
    },
  });

  // move section down one
  $(document).on('click', '#moveDown', function () {
    $.fn.fullpage.moveSectionDown();
  });

  // fullpage.js link navigation
  $(document).on('click', '#skills', function () {
    $.fn.fullpage.moveTo(2);
  });

  $(document).on('click', '#projects', function () {
    $.fn.fullpage.moveTo(3);
  });

  $(document).on('click', '#contact', function () {
    $.fn.fullpage.moveTo(4);
  });

  ('use strict');

  const modal = document.querySelector('.modal');
  const overlay = document.querySelector('.overlay');
  const btnCloseModal = document.querySelector('.close-modal');
  const btnsShowModals = document.querySelectorAll('.show-modal');
  //console.log(btnsShowModals);
  //function to open modal
  const openModal = function () {
    //console.log('button clicked');
    modal.classList.remove('hidden');
    overlay.classList.remove('hidden');
  };

  //function to close modal
  const closeModal = function () {
    modal.classList.add('hidden');
    overlay.classList.add('hidden');
  };

  //selecting modals
  for (let i = 0; i < btnsShowModals.length; i++) {
    btnsShowModals[i].addEventListener('click', openModal);

    btnCloseModal.addEventListener('click', closeModal);
    overlay.addEventListener('click', closeModal);

    //close modal through escape
    document.addEventListener('keydown', function (e) {
      //console.log(e.key);
      if (e.key === 'Escape' && !modal.classList.contains('hidden')) {
        // if (!modal.classList.contains('hidden')) {
        closeModal();
        //}
      }
    });
  }

  tippy('#award-view', {
    content(reference) {
      const id = reference.getAttribute('data-template');
      const template = document.getElementById(id);
      return template.innerHTML;
    },
    allowHTML: true,
    theme: 'light-border',
    animation: 'perspective',
  });

  tippy('.more-details', {
    content(reference) {
      return 'Click For More';
    },
    allowHTML: true,
    theme: 'light-border',
    animation: 'perspective',
  });

  $('.cog-wrapper').hover(
    function () {
      $(this).addClass('fa-spin');
    },
    function () {
      $(this).removeClass('fa-spin');
    }
  );

  // smooth scrolling
  $(function () {
    $('a[href*=#]:not([href=#])').click(function () {
      if (
        location.pathname.replace(/^\//, '') ==
          this.pathname.replace(/^\//, '') &&
        location.hostname == this.hostname
      ) {
        var target = $(this.hash);
        target = target.length
          ? target
          : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html,body').animate(
            {
              scrollTop: target.offset().top,
            },
            700
          );
          return false;
        }
      }
    });
  });

  //ajax form
  $(function () {
    // Get the form.
    var form = $('#ajax-contact');

    // Get the messages div.
    var formMessages = $('#form-messages');

    // Set up an event listener for the contact form.
    $(form).submit(function (e) {
      // Stop the browser from submitting the form.
      e.preventDefault();

      // Serialize the form data.
      var formData = $(form).serialize();

      // Submit the form using AJAX.
      $.ajax({
        type: 'POST',
        url: $(form).attr('action'),
        data: formData,
      })
        .done(function (response) {
          // Make sure that the formMessages div has the 'success' class.
          $(formMessages).removeClass('error');
          $(formMessages).addClass('success');

          // Set the message text.
          $(formMessages).text(response);

          // Clear the form.
          $('#name').val('');
          $('#email').val('');
          $('#message').val('');
        })
        .fail(function (data) {
          // Make sure that the formMessages div has the 'error' class.
          $(formMessages).removeClass('success');
          $(formMessages).addClass('error');

          // Set the message text.
          if (data.responseText !== '') {
            $(formMessages).text(data.responseText);
          } else {
            $(formMessages).text(
              'Oops! An error occured and your message could not be sent.'
            );
          }
        });
    });
  });
});
